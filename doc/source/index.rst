.. Finite element course documentation master file, created by
   sphinx-quickstart on Sat Sep  6 21:48:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Resources for Computational Science
===================================

`Kick off camp Python course <http://mpecdt.github.io/Introduction-to-Python-programming-for-MPECDT/>`_

.. toctree::
   :maxdepth: 1

   tools
   irc

Numerics course Part I (Hilary Weller)
======================================

.. toctree::
   :maxdepth: 1

   hilary/tasks

* `Good Programming Practices <http://www.met.reading.ac.uk/~sws02hs/teaching/PDEsNumerics/code.pdf>`_
    Criteria used for marking code.

* `Lecture notes <http://www.met.reading.ac.uk/~sws02hs/teaching/PDEsNumerics/PDEsNumerics_2_student.pdf>`_
    Print out these lecture notes so that you can fill in the gaps during the videos

* `Lecturer version of the notes <http://www.met.reading.ac.uk/~sws02hs/teaching/PDEsNumerics/PDEsNumerics_2_lec_growChew856.pdf>`_
    You can refer to these to check your answers (gaps filled in)

* `Discussion cite <https://mpecdtnumerics.slack.com>`_
    For asking and answering questions about the course. Use this rather than email if you want a swift response. `Sign up to the discussion cite here <https://join.slack.com/t/mpecdtnumerics/shared_invite/enQtMjQ4MTcxNDg4NDk5LTNiZWQxMDZkYjdkNWVkNTAyNjQ0YWM4ODExNWMyYWM1NzQ1MzYzOTg5OTMxODUxZWQ2ZTRhNmEyZWUxMTNmNmE>`_ before 28 October.

* `Corrections to the notes <http://www.met.reading.ac.uk/~sws02hs/teaching/PDEsNumerics/changes.pdf>`_
    If you spot errors in the notes, please report these on the discussion cite and I will give you a chocolate for your troubles. The notes will be corrected and a list of dated corrections is available `here <http://www.met.reading.ac.uk/~sws02hs/teaching/PDEsNumerics/changes.pdf>`_

* `Assignment for Part I of the Numerics course <http://www.met.reading.ac.uk/~sws02hs/teaching/PDEsNumerics/assignment.pdf>`_

..   hilary/announcements

Some background material
------------------------

* `Lecture notes and videos on using Taylor series to find finite difference approximations <https://www.youtube.com/playlist?list=PLEG35I51CH7UttDft9tOB0ej_kkAJF368>`_

* `Cubic Lagrange interpolation <http://www.met.reading.ac.uk/~sws02hs/teaching/PDEsNumerics/cubicLagrange.pdf>`_

Numerics course Part II (Colin Cotter)
======================================

* `Lecture slides (lectures 1-8) <https://share.imperial.ac.uk/fons/mathematics/mpecdt/students/Shared%20Documents/Cohort%202016/Numerical%20Methods/mpe-solver-notes.pdf>`_

* `Lecture slides (lectures 9-10) <https://share.imperial.ac.uk/fons/mathematics/mpecdt/students/Shared%20Documents/Cohort%202016/Numerical%20Methods/guest-lectures-slides.pdf>`_

* `Lecture handouts (lectures 9-10) <https://share.imperial.ac.uk/fons/mathematics/mpecdt/students/Shared%20Documents/Cohort%202016/Numerical%20Methods/guest-lectures-handouts.pdf>`_

* `Tutorial sheet 1 <https://share.imperial.ac.uk/fons/mathematics/mpecdt/students/Shared%20Documents/Cohort%202016/Numerical%20Methods/cjc-tutorial1.pdf>`_

* `Tutorial sheet 2 <https://share.imperial.ac.uk/fons/mathematics/mpecdt/students/Shared%20Documents/Cohort%202016/Numerical%20Methods/cjc-tutorial2.pdf>`_

* `Extra questions <https://share.imperial.ac.uk/fons/mathematics/mpecdt/students/Shared%20Documents/Cohort%202016/Numerical%20Methods/cjc-extra-questions.pdf>`_

* `January 2016 Exam paper <https://share.imperial.ac.uk/fons/mathematics/mpecdt/students/Shared%20Documents/Cohort%202016/Numerical%20Methods/MPE2015-NumericsExam-new.pdf>`_

* `January 2016 Exam paper with solutions for questions 3 and 4 <https://share.imperial.ac.uk/fons/mathematics/mpecdt/students/Shared%20Documents/Cohort%202016/Numerical%20Methods/MPE2015-NumericsExam-solns.pdf>`_

* `Revision list <https://share.imperial.ac.uk/fons/mathematics/mpecdt/students/Shared%20Documents/Cohort%202016/Numerical%20Methods/syllabus.pdf>`_

.. toctree::
   :maxdepth: 1

   colin/cgtutorial


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

